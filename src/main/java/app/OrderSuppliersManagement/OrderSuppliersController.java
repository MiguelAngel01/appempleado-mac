package app.OrderSuppliersManagement;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Session;
import org.hibernate.Transaction;

import app.Conexion;
import app.DeliveryManManagement.DeliveryManManagementController;
import app.InventoryManagement.InventoryManagementController;
import app.OrderManagement.OrderManagementController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import pojos.BatoiLogicProduct;
import pojos.BatoiLogicSupplier;
import pojos.BatoiLogicSupplierOrder;

public class OrderSuppliersController implements Initializable {

	ObservableList<BatoiLogicSupplierOrder> orders = FXCollections.observableArrayList();

	@FXML
	private ImageView ivLogo;
	@FXML
	private TableView<BatoiLogicSupplierOrder> tvOrdersToSupplier;
	private TableColumn<BatoiLogicSupplierOrder, Integer> tcId;
	private TableColumn<BatoiLogicSupplierOrder, BatoiLogicProduct> tcProduct;
	private TableColumn<BatoiLogicSupplierOrder, BatoiLogicSupplier> tcSupplier;
	private TableColumn<BatoiLogicSupplierOrder, Integer> tcQuantity;
	private TableColumn<BatoiLogicSupplierOrder, Double> tcPrice;

	Session s;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		s = Conexion.getSession();
		List<BatoiLogicSupplierOrder> ordersList = s
				.createQuery("from BatoiLogicSupplierOrder", BatoiLogicSupplierOrder.class).list();
		orders.addAll(ordersList);

		tcId = new TableColumn<>("ID");
		tcProduct = new TableColumn<>("Product");
		tcSupplier = new TableColumn<>("Supplier");
		tcQuantity = new TableColumn<>("Quantity");
		tcPrice = new TableColumn<>("Price");

		tcId.setCellValueFactory(new PropertyValueFactory<>("id"));
		tcProduct.setCellValueFactory(new PropertyValueFactory<>("batoiLogicProduct"));
		tcSupplier.setCellValueFactory(new PropertyValueFactory<>("batoiLogicSupplier"));
		tcQuantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
		tcPrice.setCellValueFactory(new PropertyValueFactory<>("price"));

		tvOrdersToSupplier.setItems(orders);

		tvOrdersToSupplier.getColumns().add(tcId);
		tvOrdersToSupplier.getColumns().add(tcProduct);
		tvOrdersToSupplier.getColumns().add(tcSupplier);
		tvOrdersToSupplier.getColumns().add(tcQuantity);
		tvOrdersToSupplier.getColumns().add(tcPrice);

		tvOrdersToSupplier.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
	}

	@FXML
	private void inventoryManagement() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InventoryManagementController.class.getResource("InventoryManagement.fxml"));

			BorderPane escena = loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.show();

			stage = (Stage) this.ivLogo.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void orderManagement() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(OrderManagementController.class.getResource("OrderManagement.fxml"));

			BorderPane escena = loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.show();

			stage = (Stage) this.ivLogo.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void deliveryManManagement() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(DeliveryManManagementController.class.getResource("DeliveryManManagement.fxml"));

			BorderPane escena = loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.show();

			stage = (Stage) this.ivLogo.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void receiveOrder() {
		BatoiLogicProduct product = tvOrdersToSupplier.getSelectionModel().getSelectedItem().getBatoiLogicProduct();
		product.addStock(tvOrdersToSupplier.getSelectionModel().getSelectedItem().getQuantity());
		Transaction transaction = s.beginTransaction();
		s.remove(tvOrdersToSupplier.getSelectionModel().getSelectedItem());
		transaction.commit();
		orders.remove(tvOrdersToSupplier.getSelectionModel().getSelectedItem());
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Receiving order");
		alert.setHeaderText("Updating stock");
		alert.setContentText("Proceeding to update the stock of the ordered product, The order will disappear");

		alert.showAndWait();
	}

	@FXML
	private void cancelOrder() {
		Transaction transaction = s.beginTransaction();
		s.remove(tvOrdersToSupplier.getSelectionModel().getSelectedItem());
		transaction.commit();
		orders.remove(tvOrdersToSupplier.getSelectionModel().getSelectedItem());
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Removing order");
		alert.setHeaderText("Removing order");
		alert.setContentText("The order will disappear and the stockthe stock will not update");

		alert.showAndWait();
	}

}
