package app.InventoryManagement;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Session;
import org.hibernate.query.Query;

import app.Conexion;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.stage.Stage;
import pojos.BatoiLogicProduct;
import pojos.BatoiLogicSupplier;

public class SearchArticleController implements Initializable {

	@FXML
	private ChoiceBox<BatoiLogicSupplier> cbSupplier;
	@FXML
	private Spinner<Integer> spPrice;
	@FXML
	private Spinner<Integer> spStock;

	Session s;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		s = Conexion.getSession();

		List<BatoiLogicSupplier> suppliers = s.createQuery("from BatoiLogicSupplier", BatoiLogicSupplier.class).list();
		ObservableList<BatoiLogicSupplier> suppliersList = FXCollections.observableArrayList();
		suppliersList.addAll(suppliers);
		cbSupplier.setItems(suppliersList);

		SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 200, 0);
		spStock.setValueFactory(valueFactory);
		SpinnerValueFactory<Integer> valueFactory1 = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 200, 0);
		spPrice.setValueFactory(valueFactory1);
	}

	@FXML
	private void close() {
		Stage stage = new Stage();
		stage = (Stage) this.cbSupplier.getScene().getWindow();
		stage.close();
	}

	@FXML
	private void search() {
		Query spSQLQuery;

		if (spStock.getValue() != 0) {
			spSQLQuery = s.createQuery("SELECT p FROM BatoiLogicProduct p WHERE p.stock <= :stock",
					BatoiLogicProduct.class);
			spSQLQuery.setParameter("stock", spStock.getValue());
			InventoryManagementController.productsList = spSQLQuery.list();
		} else if (spPrice.getValue() != 0) {
			spSQLQuery = s.createQuery("SELECT p FROM BatoiLogicProduct p WHERE p.price <= :price",
					BatoiLogicProduct.class);
			spSQLQuery.setParameter("price", spPrice.getValue().doubleValue());
			InventoryManagementController.productsList = spSQLQuery.list();
		} else if (cbSupplier.getValue() != null) {
			spSQLQuery = s.createQuery(
					"SELECT sp.batoiLogicProduct FROM BatoiLogicSuppliersProducts sp WHERE sp.batoiLogicSupplier = :batoiLogicSupplier");
			spSQLQuery.setParameter("batoiLogicSupplier", cbSupplier.getValue());
			InventoryManagementController.productsList = spSQLQuery.list();
		} else if (spStock.getValue() != 0 && spPrice.getValue() != 0) {
			spSQLQuery = s.createQuery(
					"SELECT p FROM BatoiLogicProduct p WHERE p.stock <= :stock AND p.price <= :price",
					BatoiLogicProduct.class);
			spSQLQuery.setParameter("stock", spStock.getValue());
			spSQLQuery.setParameter("price", spPrice.getValue().doubleValue());
			InventoryManagementController.productsList = spSQLQuery.list();
		} else if (spStock.getValue() != 0 && cbSupplier.getValue() != null) {
			spSQLQuery = s.createQuery(
					"SELECT p FROM BatoiLogicProduct p WHERE p.stock <= :stock AND sp.batoiLogicSupplier = :batoiLogicSupplier",
					BatoiLogicProduct.class);
			spSQLQuery.setParameter("stock", spStock.getValue());
			spSQLQuery.setParameter("batoiLogicSupplier", cbSupplier.getValue());
			InventoryManagementController.productsList = spSQLQuery.list();
		} else if (spStock.getValue() != 0 && cbSupplier.getValue() != null && spPrice.getValue() != 0) {
			spSQLQuery = s.createQuery(
					"SELECT p FROM BatoiLogicProduct p WHERE p.stock <= :stock AND sp.batoiLogicSupplier = :batoiLogicSupplier AND p.price <= :price",
					BatoiLogicProduct.class);
			spSQLQuery.setParameter("stock", spStock.getValue());
			spSQLQuery.setParameter("batoiLogicSupplier", cbSupplier.getValue());
			spSQLQuery.setParameter("price", spPrice.getValue().doubleValue());
			InventoryManagementController.productsList = spSQLQuery.list();
		}

		Stage stage = new Stage();
		stage = (Stage) this.cbSupplier.getScene().getWindow();
		stage.close();
	}

}
