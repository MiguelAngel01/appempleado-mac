package app.InventoryManagement;

import java.net.URL;
import java.util.ResourceBundle;

import org.hibernate.Session;
import org.hibernate.query.Query;

import app.Conexion;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;
import pojos.BatoiLogicProduct;

public class OrderArticlesController implements Initializable {

	@FXML
	private ChoiceBox<String> cbOrder;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		ObservableList<String> elements = FXCollections.observableArrayList();
		elements.addAll("ID", "Stock", "Price");
		cbOrder.setItems(elements);

	}

	@FXML
	private void close() {
		Stage stage = new Stage();
		stage = (Stage) this.cbOrder.getScene().getWindow();
		stage.close();
	}

	@FXML
	private void order() {
		Session s = Conexion.getSession();

		Query spSQLQuery;

		if (cbOrder.getValue().equals("ID")) {
			spSQLQuery = s.createQuery("SELECT p FROM BatoiLogicProduct p ORDER BY p.productId",
					BatoiLogicProduct.class);
			InventoryManagementController.productsList = spSQLQuery.list();
		} else if(cbOrder.getValue().equals("Stock")) {
			spSQLQuery = s.createQuery("SELECT p FROM BatoiLogicProduct p ORDER BY p.stock DESC",
					BatoiLogicProduct.class);
			InventoryManagementController.productsList = spSQLQuery.list();
		} else if (cbOrder.getValue().equals("Price")) {
			spSQLQuery = s.createQuery("SELECT p FROM BatoiLogicProduct p ORDER BY p.price ASC",
					BatoiLogicProduct.class);
			InventoryManagementController.productsList = spSQLQuery.list();
		}

		Stage stage = new Stage();
		stage = (Stage) this.cbOrder.getScene().getWindow();
		stage.close();
	}

}
