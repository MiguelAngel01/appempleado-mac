package app.InventoryManagement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import app.Conexion;
import app.DeliveryManManagement.DeliveryManManagementController;
import app.OrderManagement.OrderManagementController;
import app.OrderSuppliersManagement.OrderSuppliersController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pojos.BatoiLogicProduct;
import pojos.BatoiLogicSupplier;
import pojos.BatoiLogicSuppliersProducts;
import pojos.BatoiLogicSuppliersProductsId;

public class InventoryManagementController implements Initializable {

	ObservableList<BatoiLogicProduct> products = FXCollections.observableArrayList();
	static List<BatoiLogicProduct> productsList;
	public static final String SEPARATOR = ",";

	@FXML
	private ImageView ivLogo;
	@FXML
	private TableView<BatoiLogicProduct> tvInventory;
	private TableColumn<BatoiLogicProduct, String> tcProductId;
	private TableColumn<BatoiLogicProduct, String> tcName;
	private TableColumn<BatoiLogicProduct, Integer> tcStock;
	private TableColumn<BatoiLogicProduct, Integer> tcStockMin;
	private TableColumn<BatoiLogicProduct, Double> tcPrice;

	private Session s;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		s = Conexion.getSession();
		productsList = s.createQuery("from BatoiLogicProduct ORDER BY productId ASC", BatoiLogicProduct.class).list();
		products.addAll(productsList);

		tcProductId = new TableColumn<>("ID");
		tcName = new TableColumn<>("Name");
		tcStock = new TableColumn<>("Stock");
		tcStockMin = new TableColumn<>("Stock Min");
		tcPrice = new TableColumn<>("Price");

		tcProductId.setCellValueFactory(new PropertyValueFactory<>("productId"));
		tcName.setCellValueFactory(new PropertyValueFactory<>("name"));
		tcStock.setCellValueFactory(new PropertyValueFactory<>("stock"));
		tcStockMin.setCellValueFactory(new PropertyValueFactory<>("stockMin"));
		tcPrice.setCellValueFactory(new PropertyValueFactory<>("price"));

		tcName.setMinWidth(100);

		tvInventory.setItems(products);

		tvInventory.getColumns().add(tcProductId);
		tvInventory.getColumns().add(tcName);
		tvInventory.getColumns().add(tcStock);
		tvInventory.getColumns().add(tcStockMin);
		tvInventory.getColumns().add(tcPrice);

		tvInventory.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
	}

	@FXML
	private void filter() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InventoryManagementController.class.getResource("SearchArticle.fxml"));

			AnchorPane escena = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.showAndWait();
			products.removeAll(products);
			products.addAll(productsList);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void order() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InventoryManagementController.class.getResource("OrderArticles.fxml"));

			AnchorPane escena = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.showAndWait();
			products.removeAll(products);
			products.addAll(productsList);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void importCSV() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		File file = fileChooser.showOpenDialog((Stage) this.ivLogo.getScene().getWindow());

		BufferedReader br = null;

		try {
			br = new BufferedReader(new FileReader(file));
			String line = br.readLine();
			String[] fields = line.split(SEPARATOR);
			if (fields[0].equalsIgnoreCase("product") && fields[2].equalsIgnoreCase("precio")
					&& fields[1].equalsIgnoreCase("supplier")) {
				line = br.readLine();
				while (null != line) {
					fields = line.split(SEPARATOR);

					BatoiLogicSuppliersProductsId suppliersProductsId = new BatoiLogicSuppliersProductsId(
							Integer.parseInt(fields[0]), Integer.parseInt(fields[1]));

					BatoiLogicSuppliersProducts suppliersProducts = s.get(BatoiLogicSuppliersProducts.class,
							suppliersProductsId);
					Query query = s.createQuery("FROM BatoiLogicProduct WHERE productId = :productId");
					query.setParameter("productId", Integer.parseInt(fields[0]));
					BatoiLogicProduct product = (BatoiLogicProduct) query.list().get(0);

					if (suppliersProducts == null) {
						BatoiLogicSupplier supplier = s.get(BatoiLogicSupplier.class, Integer.parseInt(fields[1]));
						suppliersProducts = new BatoiLogicSuppliersProducts(suppliersProductsId, product, supplier,
								Double.parseDouble(fields[2]));
					} else {
						suppliersProducts.setBatoiLogicProduct(product);
						suppliersProducts.setPrice(Double.parseDouble(fields[2]));
					}

					product.setPrice(Double.parseDouble(fields[2]));

					Transaction transaction = s.beginTransaction();
					transaction.begin();
					s.save(suppliersProducts);
					s.save(product);
					transaction.commit();

					line = br.readLine();
				}
			} else if (fields[0].equalsIgnoreCase("producto") && fields[1].equalsIgnoreCase("precio")) {
				System.out.println(fields[0] + " " + fields[1]);
				String supplier = file.getName().substring(file.getName().length() - 6, file.getName().length() - 4);

				int supplierId = Integer.parseInt(supplier);

				line = br.readLine();
				while (null != line) {
					fields = line.split(SEPARATOR);
					System.out.println(fields[0] + " " + fields[1]);

					BatoiLogicSupplier supplierObject = s.get(BatoiLogicSupplier.class, supplierId);
					
					Query query = s.createQuery("SELECT sp.id FROM BatoiLogicSuppliersProducts sp WHERE sp.batoiLogicSupplier = :batoiLogicSupplier", BatoiLogicSuppliersProductsId.class);
					query.setParameter("batoiLogicSupplier", supplierObject);
					BatoiLogicSuppliersProductsId suppliersProductsId = (BatoiLogicSuppliersProductsId) query.list().get(0);
					BatoiLogicSuppliersProducts suppliersProducts = s.get(BatoiLogicSuppliersProducts.class,
							suppliersProductsId);
					query = s.createQuery("FROM BatoiLogicProduct WHERE productId = :productId", BatoiLogicProduct.class);
					query.setParameter("productId", Integer.parseInt(fields[0]));
					BatoiLogicProduct product = (BatoiLogicProduct) query.list().get(0);

					suppliersProducts.setBatoiLogicProduct(product);
					suppliersProducts.setPrice(Double.parseDouble(fields[1]));

					product.setPrice(Double.parseDouble(fields[1]));

					Transaction transaction = s.beginTransaction();
					//transaction.begin();
					s.save(suppliersProducts);
					s.save(product);
					transaction.commit();

					line = br.readLine();
				}
			} else {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Couldn't load csv");
				alert.setHeaderText("CSV is not valid");
				alert.setContentText(
						"The import is only for suppliers prices, \nthe structure of your csv must be \nproduct,supplier,price and producto,precio");

				alert.showAndWait();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != br) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	@FXML
	private void orderManagement() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(OrderManagementController.class.getResource("OrderManagement.fxml"));

			BorderPane escena = loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.show();

			stage = (Stage) this.ivLogo.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void deliveryManManagement() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(DeliveryManManagementController.class.getResource("DeliveryManManagement.fxml"));

			BorderPane escena = loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.show();

			stage = (Stage) this.ivLogo.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void orderSuppliers() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(OrderSuppliersController.class.getResource("OrdersSuppliers.fxml"));

			BorderPane escena = loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.show();

			stage = (Stage) this.ivLogo.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
