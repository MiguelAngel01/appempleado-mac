package app.OrderManagement;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.byteowls.jopencage.JOpenCageGeocoder;
import com.byteowls.jopencage.model.JOpenCageForwardRequest;
import com.byteowls.jopencage.model.JOpenCageLatLng;
import com.byteowls.jopencage.model.JOpenCageResponse;

import app.Conexion;
import app.DeliveryManManagement.DeliveryManManagementController;
import app.InventoryManagement.InventoryManagementController;
import app.OrderSuppliersManagement.OrderSuppliersController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import pojos.BatoiLogicAddress;
import pojos.BatoiLogicBill;
import pojos.BatoiLogicCity;
import pojos.BatoiLogicCustomer;
import pojos.BatoiLogicDeliveryNote;
import pojos.BatoiLogicDeliveryPerson;
import pojos.BatoiLogicOrder;
import pojos.BatoiLogicPostalCode;
import pojos.BatoiLogicRoute;
import pojos.BatoiLogicTruck;

public class OrderManagementController implements Initializable {

	// TODO: Crear ruta

	ObservableList<BatoiLogicOrder> orders = FXCollections.observableArrayList();
	static BatoiLogicOrder order;

	List<BatoiLogicDeliveryNote> deliveryNotes;
	List<BatoiLogicBill> bills;

	@FXML
	private ImageView ivLogo;
	@FXML
	private TableView<BatoiLogicOrder> tvOrders;
	private TableColumn<BatoiLogicOrder, Integer> tcId;
	private TableColumn<BatoiLogicOrder, BatoiLogicAddress> tcAddress;
	private TableColumn<BatoiLogicOrder, Integer> tcCustomer;

	Session s;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		s = Conexion.getSession();
		List<BatoiLogicOrder> ordersList = s.createQuery("from BatoiLogicOrder", BatoiLogicOrder.class).list();
		deliveryNotes = s.createQuery("from BatoiLogicDeliveryNote", BatoiLogicDeliveryNote.class).list();
		bills = s.createQuery("from BatoiLogicBill", BatoiLogicBill.class).list();
		orders.addAll(ordersList);

		tcId = new TableColumn<>("ID");
		tcAddress = new TableColumn<>("Address");
		tcCustomer = new TableColumn<>("Customer");

		tcId.setCellValueFactory(new PropertyValueFactory<>("id"));
		tcAddress.setCellValueFactory(new PropertyValueFactory<>("batoiLogicAddress"));
		tcCustomer.setCellValueFactory(new PropertyValueFactory<>("customerId"));

		tvOrders.setItems(orders);

		tvOrders.getColumns().add(tcId);
		tvOrders.getColumns().add(tcCustomer);
		tvOrders.getColumns().add(tcAddress);

		tvOrders.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
	}

	@FXML
	private void seeDetails() {

		try {

			order = tvOrders.getSelectionModel().getSelectedItem();

			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(OrderManagementController.class.getResource("OrderDetails.fxml"));

			AnchorPane escena = loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@FXML
	private void createRoute() {

		BatoiLogicAddress address = tvOrders.getSelectionModel().getSelectedItem().getBatoiLogicAddress();
		Set<BatoiLogicDeliveryNote> notes = tvOrders.getSelectionModel().getSelectedItem().getBatoiLogicDeliveryNotes();
		if (notes.size() <= 0) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Couldn't create route");
			alert.setHeaderText("Delivery note doesn't found");
			alert.setContentText("You need to create a delivery note previously to create a route");

			alert.showAndWait();
		} else {
			JOpenCageGeocoder jOpenCageGeocoder = new JOpenCageGeocoder("3af3ad1eeeb34934a37323a574b1c546");
			BatoiLogicPostalCode postalCode = address.getBatoiLogicPostalCode();
			BatoiLogicCity city = postalCode.getBatoiLogicCity();
			JOpenCageForwardRequest request = new JOpenCageForwardRequest(address.getName() + ", " + city.getName()
					+ ", " + postalCode.getProvince() + ", " + postalCode.getPostalCodeId());
			request.setRestrictToCountryCode("es");

			JOpenCageResponse response = jOpenCageGeocoder.forward(request);
			JOpenCageLatLng firstResultLatLng = response.getFirstPosition();
			Transaction transaction = s.beginTransaction();
			List<BatoiLogicRoute> routes = s.createQuery("from BatoiLogicRoute", BatoiLogicRoute.class).list();
			BatoiLogicRoute route;
			if(routes.size() > 0) {
				route = new BatoiLogicRoute(routes.get(routes.size() - 1).getId() + 1, firstResultLatLng.getLat(),
						firstResultLatLng.getLng(), (BatoiLogicDeliveryNote) notes.toArray()[0]);
			} else {
				route = new BatoiLogicRoute(1, firstResultLatLng.getLat(),
						firstResultLatLng.getLng(), (BatoiLogicDeliveryNote) notes.toArray()[0]);
			}
			
			BatoiLogicDeliveryNote note = (BatoiLogicDeliveryNote) notes.toArray()[0];
			note.setBatoiLogicRoute(route);
			s.save(route);
			s.save(note);
			transaction.commit();
		}
	}

	@FXML
	private void createDeliveryNote() {

		BatoiLogicCustomer customer = s.get(BatoiLogicCustomer.class,
				tvOrders.getSelectionModel().getSelectedItem().getCustomerId());
		BatoiLogicOrder order = tvOrders.getSelectionModel().getSelectedItem();

		Transaction transaction = s.beginTransaction();
		BatoiLogicDeliveryNote batoiLogicDeliveryNote;
		if(deliveryNotes.size() > 0) {
			batoiLogicDeliveryNote = new BatoiLogicDeliveryNote(deliveryNotes.get(deliveryNotes.size() - 1).getId() + 1, order,
					customer.getName() + " " + customer.getLastName(), customer.getNif());
		} else {
			batoiLogicDeliveryNote = new BatoiLogicDeliveryNote(1, order,
					customer.getName() + " " + customer.getLastName(), customer.getNif());
		}
		order.setBatoiLogicDeliveryNote(batoiLogicDeliveryNote);
		s.save(batoiLogicDeliveryNote);
		s.save(order);
		transaction.commit();

	}

	@FXML
	private void createBill() {

		BatoiLogicDeliveryNote deliveryNote = tvOrders.getSelectionModel().getSelectedItem()
				.getBatoiLogicDeliveryNote();
		if (deliveryNote == null) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Couldn't create a bill");
			alert.setHeaderText("Delivery note doesn't found");
			alert.setContentText("You need to create a delivery note previously to create a bill");

			alert.showAndWait();
		} else {
			for (BatoiLogicBill batoiLogicBill : bills) {
				if (batoiLogicBill.getBatoiLogicDeliveryNote() == deliveryNote) {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Couldn't create a bill");
					alert.setHeaderText("Bill for this delivery note already exists");
					alert.setContentText("You couldn't create more than one bill for a one delivery note,\nif you want to create a new bill delete the actual bill");

					alert.showAndWait();
					return;
				}
			}
			BatoiLogicAddress address = tvOrders.getSelectionModel().getSelectedItem().getBatoiLogicAddress();

			Transaction transaction = s.beginTransaction();
			BatoiLogicBill batoiLogicBill;
			if(bills.size() > 0) {
				batoiLogicBill = new BatoiLogicBill(bills.get(bills.size() - 1).getId() + 1, address, deliveryNote);
			} else {
				batoiLogicBill = new BatoiLogicBill(1, address, deliveryNote);
			}
			s.save(batoiLogicBill);
			transaction.commit();
		}

	}

	@FXML
	private void inventoryManagement() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InventoryManagementController.class.getResource("InventoryManagement.fxml"));

			BorderPane escena = loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.show();

			stage = (Stage) this.ivLogo.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void deliveryManManagement() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(DeliveryManManagementController.class.getResource("DeliveryManManagement.fxml"));

			BorderPane escena = loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.show();

			stage = (Stage) this.ivLogo.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void orderSuppliers() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(OrderSuppliersController.class.getResource("OrdersSuppliers.fxml"));

			BorderPane escena = loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.show();

			stage = (Stage) this.ivLogo.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
