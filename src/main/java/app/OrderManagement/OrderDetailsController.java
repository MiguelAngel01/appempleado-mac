package app.OrderManagement;

import java.awt.Button;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Query;
import org.hibernate.Session;
import app.Conexion;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pojos.BatoiLogicOrder;
import pojos.BatoiLogicOrderLine;
import pojos.BatoiLogicProduct;

public class OrderDetailsController implements Initializable{
	
	ObservableList<BatoiLogicOrderLine> orderLines = FXCollections.observableArrayList();
	
	@FXML
	AnchorPane scene;
	@FXML
	private TableView<BatoiLogicOrderLine> tvOrderLine;
	private TableColumn<BatoiLogicOrderLine, Integer> tcLine;
	private TableColumn<BatoiLogicOrderLine, BatoiLogicOrder> tvOrder;
	private TableColumn<BatoiLogicOrderLine, BatoiLogicProduct> tvProduct;
	private TableColumn<BatoiLogicOrderLine, Integer> tcQuantity;
	private TableColumn<BatoiLogicOrderLine, Double> tcPrice;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		Session s = Conexion.getSession();
		Query query = s.createQuery("from BatoiLogicOrderLine where batoiLogicOrder = :batoiLogicOrder", BatoiLogicOrderLine.class);
		query.setParameter("batoiLogicOrder", OrderManagementController.order);
		List<BatoiLogicOrderLine> orderLinesList = query.list();
		orderLines.addAll(orderLinesList);
		
		tcLine = new TableColumn<>("Line");
		tvOrder = new TableColumn<>("Order");
		tvProduct = new TableColumn<>("Product");
		tcQuantity = new TableColumn<>("Quantity");
		tcPrice = new TableColumn<>("Price");
		
		tcLine.setCellValueFactory(new PropertyValueFactory<>("line"));
		tvOrder.setCellValueFactory(new PropertyValueFactory<>("batoiLogicOrder"));
		tvProduct.setCellValueFactory(new PropertyValueFactory<>("batoiLogicProduct"));
		tcQuantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
		tcPrice.setCellValueFactory(new PropertyValueFactory<>("price"));
		
		tvProduct.setMinWidth(150);
		
		tvOrderLine.setItems(orderLines);
		
		tvOrderLine.getColumns().add(tcLine);
		tvOrderLine.getColumns().add(tvOrder);
		tvOrderLine.getColumns().add(tvProduct);
		tvOrderLine.getColumns().add(tcQuantity);
		tvOrderLine.getColumns().add(tcPrice);
		
		tvOrderLine.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
	}

	@FXML
	private void close() {
		Stage stage = new Stage();      
        stage = (Stage) this.scene.getScene().getWindow();
        stage.close();
	}
	
}
