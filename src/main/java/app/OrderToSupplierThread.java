package app;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import pojos.BatoiLogicProduct;
import pojos.BatoiLogicSupplier;
import pojos.BatoiLogicSupplierOrder;
import pojos.BatoiLogicSuppliersProducts;

public class OrderToSupplierThread extends Thread {

	List<BatoiLogicProduct> productsList;

	Session s;

	public OrderToSupplierThread() {
		s = Conexion.getSession();
	}

	@Override
	public void run() {

		while (true) {
			productsList = s.createQuery("from BatoiLogicProduct", BatoiLogicProduct.class).list();

			for (BatoiLogicProduct batoiLogicProduct : productsList) {
				if (batoiLogicProduct.getStock() <= batoiLogicProduct.getStockMin()) {

					Query<BatoiLogicSupplierOrder> query = s.createQuery(
							"FROM BatoiLogicSupplierOrder so WHERE so.batoiLogicProduct = :batoiLogicProduct",
							BatoiLogicSupplierOrder.class);
					query.setParameter("batoiLogicProduct", batoiLogicProduct);
					List<BatoiLogicSupplierOrder> supplierOrders = query.list();
					if (supplierOrders.size() <= 0) {
						makeOrder(batoiLogicProduct);
					} else if (batoiLogicProduct != supplierOrders.get(0).getBatoiLogicProduct()) {
						makeOrder(batoiLogicProduct);
					}

				}
			}
			try {
				sleep(300000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	private void makeOrder(BatoiLogicProduct batoiLogicProduct) {
		Query<BatoiLogicSuppliersProducts> query = s.createQuery(
				"FROM BatoiLogicSuppliersProducts sp WHERE sp.batoiLogicProduct = :batoiLogicProduct",
				BatoiLogicSuppliersProducts.class);
		query.setParameter("batoiLogicProduct", batoiLogicProduct);
		List<BatoiLogicSuppliersProducts> suppliersProducts = query.list();
		Double price = suppliersProducts.get(0).getPrice();
		BatoiLogicSupplier batoiLogicSupplier = suppliersProducts.get(0).getBatoiLogicSupplier();

		List<BatoiLogicSupplierOrder> supplierOrderList = s
				.createQuery("FROM BatoiLogicSupplierOrder", BatoiLogicSupplierOrder.class).list();

		Integer quantity = batoiLogicProduct.getStockMin() - batoiLogicProduct.getStock() + 10;

		Transaction transaction = s.beginTransaction();
		BatoiLogicSupplierOrder supplierOrder = new BatoiLogicSupplierOrder(supplierOrderList.size() + 1,
				batoiLogicProduct, batoiLogicSupplier, quantity, quantity * price);
		s.save(supplierOrder);
		transaction.commit();
	}

}
