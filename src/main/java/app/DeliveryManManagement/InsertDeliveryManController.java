package app.DeliveryManManagement;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import app.Conexion;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import pojos.BatoiLogicDeliveryPerson;
import pojos.BatoiLogicTruck;

public class InsertDeliveryManController implements Initializable {

	List<BatoiLogicTruck> trucksList;
	List<BatoiLogicDeliveryPerson> deliveryMans;

	@FXML
	private TextField tfName;
	@FXML
	private TextField tfLicensePlate;
	@FXML
	private TextField tfNIF;

	Session s;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		s = Conexion.getSession();

		trucksList = s.createQuery("from BatoiLogicTruck t", BatoiLogicTruck.class).list();
		deliveryMans = s.createQuery("from BatoiLogicDeliveryPerson d", BatoiLogicDeliveryPerson.class).list();

	}

	@FXML
	private void insert() {
		try {
			Transaction transaction = s.beginTransaction();
			Query query;
			query = s.createQuery("select t from BatoiLogicTruck t WHERE carLicensePlate = :carLicensePlate",
					BatoiLogicTruck.class);
			query.setParameter("carLicensePlate", tfLicensePlate.getText());
			BatoiLogicTruck truck = (BatoiLogicTruck) query.list().get(0);
			BatoiLogicDeliveryPerson deliveryMan;
			if (deliveryMans.size() > 0) {
				deliveryMan = new BatoiLogicDeliveryPerson(
						deliveryMans.get(deliveryMans.size() - 1).getId() + 1, tfName.getText(), tfNIF.getText(),
						truck);
			} else {
				deliveryMan = new BatoiLogicDeliveryPerson(1, tfName.getText(),
						tfNIF.getText(), truck);
			}

			s.save(deliveryMan);
			transaction.commit();

			DeliveryManManagementController.deliveryMans.add(deliveryMan);

			Stage stage = (Stage) tfName.getScene().getWindow();
			stage.close();
		} catch (IndexOutOfBoundsException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Couldn't load truck");
			alert.setHeaderText("Impossible to create the delivery man");
			alert.setContentText("You need to create a truck or indicate one that already exists");

			alert.showAndWait();
		}
	}

	@FXML
	private void close() {
		Stage stage = (Stage) tfName.getScene().getWindow();
		stage.close();
	}

	@FXML
	private void createTruck() {
		Query query;
		query = s.createQuery("select t from BatoiLogicTruck t WHERE carLicensePlate = :carLicensePlate",
				BatoiLogicTruck.class);
		query.setParameter("carLicensePlate", tfLicensePlate.getText());
		List<BatoiLogicTruck> trucks = query.list();
		if (trucks.size() == 0) {
			Transaction transaction = s.beginTransaction();
			BatoiLogicTruck truck = new BatoiLogicTruck(trucksList.size() + 1, tfLicensePlate.getText());
			s.save(truck);
			transaction.commit();
		}

	}

}
