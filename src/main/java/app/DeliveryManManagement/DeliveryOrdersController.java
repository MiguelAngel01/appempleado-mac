package app.DeliveryManManagement;

import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.hibernate.Session;
import app.Conexion;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import pojos.BatoiLogicAddress;
import pojos.BatoiLogicDeliveryNote;
import pojos.BatoiLogicOrder;
import pojos.BatoiLogicRoute;

public class DeliveryOrdersController implements Initializable {
	ObservableList<BatoiLogicOrder> orders = FXCollections.observableArrayList();

	@FXML
	private TableView<BatoiLogicOrder> tvOrders;
	private TableColumn<BatoiLogicOrder, Integer> tcId;
	private TableColumn<BatoiLogicOrder, BatoiLogicAddress> tcAddress;
	private TableColumn<BatoiLogicOrder, Integer> tcCustomer;
	
	Session s;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		s = Conexion.getSession();
		
		Set<BatoiLogicRoute> routes = DeliveryManManagementController.deliveryPerson.getBatoiLogicRoutes();
		
		Set<BatoiLogicDeliveryNote> notes = new HashSet<BatoiLogicDeliveryNote>();
		for(BatoiLogicRoute route : routes) {
			notes.addAll(route.getBatoiLogicDeliveryNotes());
		}
		
		Set<BatoiLogicOrder> ordersList = new HashSet<BatoiLogicOrder>();
		for(BatoiLogicDeliveryNote note : notes) {
			ordersList.add(note.getBatoiLogicOrder());
		}
		
		orders.addAll(ordersList);
		
		tcId = new TableColumn<>("ID");
		tcAddress = new TableColumn<>("Address");
		tcCustomer = new TableColumn<>("Customer");

		tcId.setCellValueFactory(new PropertyValueFactory<>("id"));
		tcAddress.setCellValueFactory(new PropertyValueFactory<>("batoiLogicAddress"));
		tcCustomer.setCellValueFactory(new PropertyValueFactory<>("customerId"));
		
		tcAddress.setMinWidth(150);

		tvOrders.setItems(orders);

		tvOrders.getColumns().add(tcId);
		tvOrders.getColumns().add(tcCustomer);
		tvOrders.getColumns().add(tcAddress);

		tvOrders.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
	}

	@FXML
	private void close() {
		Stage stage = (Stage) this.tvOrders.getScene().getWindow();
        stage.close();
	}
	
}
