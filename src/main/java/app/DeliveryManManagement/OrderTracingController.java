package app.DeliveryManManagement;

import java.net.URL;
import java.util.ResourceBundle;

import org.hibernate.Session;
import org.hibernate.query.Query;

import com.dlsc.gmapsfx.GoogleMapView;
import com.dlsc.gmapsfx.MapComponentInitializedListener;
import com.dlsc.gmapsfx.javascript.object.GoogleMap;
import com.dlsc.gmapsfx.javascript.object.LatLong;
import com.dlsc.gmapsfx.javascript.object.MapOptions;
import com.dlsc.gmapsfx.javascript.object.MapTypeIdEnum;
import com.dlsc.gmapsfx.javascript.object.Marker;
import com.dlsc.gmapsfx.javascript.object.MarkerOptions;

import app.Conexion;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import pojos.BatoiLogicDeliveryPerson;
import pojos.BatoiLogicLocation;

public class OrderTracingController implements Initializable, MapComponentInitializedListener {

	@FXML
	private GoogleMapView gmapsfx;
	private GoogleMap map;

	Session s;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		s = Conexion.getSession();
		gmapsfx.addMapInitializedListener(this);
	}

	@FXML
	private void close() {
		Stage stage = (Stage) this.gmapsfx.getScene().getWindow();
		stage.close();
	}

	@Override
	public void mapInitialized() {
		MapOptions mapOptions = new MapOptions();

		BatoiLogicDeliveryPerson deliveryPerson = DeliveryManManagementController.deliveryPerson;
		Query<BatoiLogicLocation> query = s.createQuery(
				"SELECT l FROM BatoiLogicLocation l WHERE l.batoiLogicDeliveryPerson = :batoiLogicDeliveryPerson",
				BatoiLogicLocation.class);
		query.setParameter("batoiLogicDeliveryPerson", deliveryPerson);
		
		if (query.list().size() > 0) {
			BatoiLogicLocation location = query.list().get(0);
			mapOptions
					.center(new LatLong(Double.parseDouble(location.getLatitude()),
							Double.parseDouble(location.getLongitude())))
					.mapType(MapTypeIdEnum.ROADMAP).overviewMapControl(false).panControl(false).rotateControl(false)
					.scaleControl(false).streetViewControl(false).zoomControl(true).zoom(17);

			gmapsfx.setKey("AIzaSyA5x2E_ntWXncA0Y3DjHx2H_zh6gxOWCaQ");
			map = gmapsfx.createMap(mapOptions);

			// Add a marker to the map
			MarkerOptions markerOptions = new MarkerOptions();

			markerOptions.position(new LatLong(Double.parseDouble(location.getLatitude()),
					Double.parseDouble(location.getLongitude()))).visible(Boolean.TRUE).title("My Marker");

			Marker marker = new Marker(markerOptions);

			map.addMarker(marker);
		} else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Couldn't load location");
			alert.setHeaderText("Problem retrieving location");
			alert.setContentText("The delivery man location couldn't be retrieved");

			alert.showAndWait();
			Stage stage = (Stage) this.gmapsfx.getScene().getWindow();
			stage.close();
		}

	}

}
