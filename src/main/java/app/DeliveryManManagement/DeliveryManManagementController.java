package app.DeliveryManManagement;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import org.hibernate.Session;
import org.hibernate.Transaction;

import app.Conexion;
import app.InventoryManagement.InventoryManagementController;
import app.OrderManagement.OrderManagementController;
import app.OrderSuppliersManagement.OrderSuppliersController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import pojos.BatoiLogicDeliveryPerson;
import pojos.BatoiLogicTruck;

public class DeliveryManManagementController implements Initializable {
	
	static ObservableList<BatoiLogicDeliveryPerson> deliveryMans = FXCollections.observableArrayList();
	List<BatoiLogicDeliveryPerson> deliveryMansList;
	
	static BatoiLogicDeliveryPerson deliveryPerson;
	
	@FXML
	private ImageView ivLogo;
	@FXML
	private TableView<BatoiLogicDeliveryPerson> tvDelivery;
	private TableColumn<BatoiLogicDeliveryPerson, Integer> tcId;
	private TableColumn<BatoiLogicDeliveryPerson, BatoiLogicTruck> tcTruck;
	private TableColumn<BatoiLogicDeliveryPerson, String> tcName;
	private TableColumn<BatoiLogicDeliveryPerson, String> tcNif;

	Session s;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		s = Conexion.getSession();
		deliveryMansList = s.createQuery("from BatoiLogicDeliveryPerson", BatoiLogicDeliveryPerson.class).list();
		deliveryMans.removeAll(deliveryMans);
		deliveryMans.addAll(deliveryMansList);

		tcId = new TableColumn<>("ID");
		tcName = new TableColumn<>("Name");
		tcTruck = new TableColumn<>("Truck");
		tcNif = new TableColumn<>("NIF");
		
		tcId.setCellValueFactory(new PropertyValueFactory<>("id"));
		tcName.setCellValueFactory(new PropertyValueFactory<>("name"));
		tcTruck.setCellValueFactory(new PropertyValueFactory<>("batoiLogicTruck"));
		tcNif.setCellValueFactory(new PropertyValueFactory<>("nif"));
		
		tvDelivery.setItems(deliveryMans);
		
		tvDelivery.getColumns().add(tcId);
		tvDelivery.getColumns().add(tcName);
		tvDelivery.getColumns().add(tcTruck);
		tvDelivery.getColumns().add(tcNif);
		
		tvDelivery.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
	}

	@FXML
	private void assignRoute() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(DeliveryManManagementController.class.getResource("AssignRoute.fxml"));

			AnchorPane escena = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void orderTracing() {
		try {
			deliveryPerson = tvDelivery.getSelectionModel().getSelectedItem();
			
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(DeliveryManManagementController.class.getResource("OrderTracing.fxml"));

			AnchorPane escena = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void seeOrders() {
		try {
			
			deliveryPerson = tvDelivery.getSelectionModel().getSelectedItem();
			
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(DeliveryManManagementController.class.getResource("DeliveryOrders.fxml"));

			AnchorPane escena = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void insert() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(DeliveryManManagementController.class.getResource("InsertDeliveryMan.fxml"));

			AnchorPane escena = loader.load();
			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.showAndWait();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void delete() {
		Transaction transaction = s.beginTransaction();
		BatoiLogicDeliveryPerson deliveryPerson = tvDelivery.getSelectionModel().getSelectedItem();
		s.remove(deliveryPerson);
		transaction.commit();
		deliveryMans.remove(tvDelivery.getSelectionModel().getSelectedItem());
		
	}
	
	@FXML
	private void inventoryManagement() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(InventoryManagementController.class.getResource("InventoryManagement.fxml"));

			BorderPane escena = loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.show();

			stage = (Stage) this.ivLogo.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void orderManagement() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(OrderManagementController.class.getResource("OrderManagement.fxml"));

			BorderPane escena = loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.show();

			stage = (Stage) this.ivLogo.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void orderSuppliers() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(OrderSuppliersController.class.getResource("OrdersSuppliers.fxml"));

			BorderPane escena = loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(escena));
			stage.show();

			stage = (Stage) this.ivLogo.getScene().getWindow();
			stage.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
