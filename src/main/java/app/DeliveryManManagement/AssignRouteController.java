package app.DeliveryManManagement;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Session;
import org.hibernate.Transaction;

import app.Conexion;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import pojos.BatoiLogicDeliveryPerson;
import pojos.BatoiLogicRoute;

public class AssignRouteController implements Initializable {

	@FXML
	private ChoiceBox<BatoiLogicDeliveryPerson> cbDeliveryMan;
	@FXML
	private ChoiceBox<BatoiLogicRoute> cbRoute;
	
	Session s;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		s = Conexion.getSession();
		List<BatoiLogicDeliveryPerson> deliveryMans = s.createQuery("FROM BatoiLogicDeliveryPerson", BatoiLogicDeliveryPerson.class).list();
		ObservableList<BatoiLogicDeliveryPerson> deliveryMansList = FXCollections.observableArrayList();
		deliveryMansList.addAll(deliveryMans);
		cbDeliveryMan.setItems(deliveryMansList);
		
		List<BatoiLogicRoute> routes = s.createQuery("FROM BatoiLogicRoute", BatoiLogicRoute.class).list();
		ObservableList<BatoiLogicRoute> routesList = FXCollections.observableArrayList();
		routesList.addAll(routes);
		cbRoute.setItems(routesList);

	}

	@FXML
	private void assign() {
		
		//Transaction transaction = s.beginTransaction();
		cbDeliveryMan.getValue().addRoute(cbRoute.getValue());
		cbRoute.getValue().setBatoiLogicDeliveryPerson(cbDeliveryMan.getValue());
		s.save(cbDeliveryMan.getValue());
		s.save(cbRoute.getValue());
		//transaction.commit();
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Route created");
		alert.setHeaderText("Route created without errors");
		alert.setContentText("The route " + cbRoute.getValue() + " assigned to " + cbDeliveryMan.getValue());

		alert.showAndWait();
		
		Stage stage = (Stage) this.cbDeliveryMan.getScene().getWindow();
        stage.close();        
	}
	
	@FXML
	private void close() {
		Stage stage = (Stage) this.cbDeliveryMan.getScene().getWindow();
        stage.close();
	}
	
}
