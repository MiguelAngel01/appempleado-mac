package pojos;
// Generated 15 feb. 2021 19:18:09 by Hibernate Tools 5.2.12.Final

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * BatoiLogicSuppliersProductsId generated by hbm2java
 */
@Embeddable
public class BatoiLogicSuppliersProductsId implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int batoiLogicProductId;
	private int batoiLogicSupplierId;

	public BatoiLogicSuppliersProductsId() {
	}

	public BatoiLogicSuppliersProductsId(int batoiLogicProductId, int batoiLogicSupplierId) {
		this.batoiLogicProductId = batoiLogicProductId;
		this.batoiLogicSupplierId = batoiLogicSupplierId;
	}

	@Column(name = "batoi_logic_product_id", nullable = false)
	public int getBatoiLogicProductId() {
		return this.batoiLogicProductId;
	}

	public void setBatoiLogicProductId(int batoiLogicProductId) {
		this.batoiLogicProductId = batoiLogicProductId;
	}

	@Column(name = "batoi_logic_supplier_id", nullable = false)
	public int getBatoiLogicSupplierId() {
		return this.batoiLogicSupplierId;
	}

	public void setBatoiLogicSupplierId(int batoiLogicSupplierId) {
		this.batoiLogicSupplierId = batoiLogicSupplierId;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof BatoiLogicSuppliersProductsId))
			return false;
		BatoiLogicSuppliersProductsId castOther = (BatoiLogicSuppliersProductsId) other;

		return (this.getBatoiLogicProductId() == castOther.getBatoiLogicProductId())
				&& (this.getBatoiLogicSupplierId() == castOther.getBatoiLogicSupplierId());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getBatoiLogicProductId();
		result = 37 * result + this.getBatoiLogicSupplierId();
		return result;
	}

}
