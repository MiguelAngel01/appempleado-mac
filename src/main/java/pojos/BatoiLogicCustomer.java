package pojos;
// Generated 12 feb. 2021 19:22:09 by Hibernate Tools 5.2.12.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * BatoiLogicCustomer generated by hbm2java
 */
@Entity
@Table(name = "batoi_logic_customer", schema = "public")
public class BatoiLogicCustomer implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String lastName;
	private String nif;
	private Integer telephoneNumber;
	private String mail;
	private String password;
	private Integer createUid;
	private Date createDate;
	private Integer writeUid;
	private Date writeDate;
	private Set<BatoiLogicAddress> batoiLogicAddresses = new HashSet<BatoiLogicAddress>(0);

	public BatoiLogicCustomer() {
	}

	public BatoiLogicCustomer(int id) {
		this.id = id;
	}

	public BatoiLogicCustomer(int id, String name, String lastName, String nif, Integer telephoneNumber, String mail,
			String password, Integer createUid, Date createDate, Integer writeUid, Date writeDate,
			Set<BatoiLogicAddress> batoiLogicAddresses) {
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.nif = nif;
		this.telephoneNumber = telephoneNumber;
		this.mail = mail;
		this.password = password;
		this.createUid = createUid;
		this.createDate = createDate;
		this.writeUid = writeUid;
		this.writeDate = writeDate;
		this.batoiLogicAddresses = batoiLogicAddresses;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "last_name")
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "nif")
	public String getNif() {
		return this.nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	@Column(name = "telephone_number")
	public Integer getTelephoneNumber() {
		return this.telephoneNumber;
	}

	public void setTelephoneNumber(Integer telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	@Column(name = "mail")
	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@Column(name = "password")
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "create_uid")
	public Integer getCreateUid() {
		return this.createUid;
	}

	public void setCreateUid(Integer createUid) {
		this.createUid = createUid;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 29)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "write_uid")
	public Integer getWriteUid() {
		return this.writeUid;
	}

	public void setWriteUid(Integer writeUid) {
		this.writeUid = writeUid;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "write_date", length = 29)
	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "batoiLogicCustomer")
	public Set<BatoiLogicAddress> getBatoiLogicAddresses() {
		return this.batoiLogicAddresses;
	}

	public void setBatoiLogicAddresses(Set<BatoiLogicAddress> batoiLogicAddresses) {
		this.batoiLogicAddresses = batoiLogicAddresses;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name;
	}

}
