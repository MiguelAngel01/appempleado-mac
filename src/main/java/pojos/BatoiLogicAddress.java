package pojos;
// Generated 12 feb. 2021 19:22:09 by Hibernate Tools 5.2.12.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * BatoiLogicAddress generated by hbm2java
 */
@Entity
@Table(name = "batoi_logic_address", schema = "public")
public class BatoiLogicAddress implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private BatoiLogicCustomer batoiLogicCustomer;
	private BatoiLogicPostalCode batoiLogicPostalCode;
	private String name;
	private Integer createUid;
	private Date createDate;
	private Integer writeUid;
	private Date writeDate;
	private Set<BatoiLogicOrder> batoiLogicOrders = new HashSet<BatoiLogicOrder>(0);
	private Set<BatoiLogicBill> batoiLogicBills = new HashSet<BatoiLogicBill>(0);

	public BatoiLogicAddress() {
	}

	public BatoiLogicAddress(int id) {
		this.id = id;
	}

	public BatoiLogicAddress(int id, BatoiLogicCustomer batoiLogicCustomer, BatoiLogicPostalCode batoiLogicPostalCode,
			String name, Integer createUid, Date createDate, Integer writeUid, Date writeDate, Set<BatoiLogicOrder> batoiLogicOrders,
			Set<BatoiLogicBill> batoiLogicBills) {
		this.id = id;
		this.batoiLogicCustomer = batoiLogicCustomer;
		this.batoiLogicPostalCode = batoiLogicPostalCode;
		this.name = name;
		this.createUid = createUid;
		this.createDate = createDate;
		this.writeUid = writeUid;
		this.writeDate = writeDate;
		this.batoiLogicOrders = batoiLogicOrders;
		this.batoiLogicBills = batoiLogicBills;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customer_id")
	public BatoiLogicCustomer getBatoiLogicCustomer() {
		return this.batoiLogicCustomer;
	}

	public void setBatoiLogicCustomer(BatoiLogicCustomer batoiLogicCustomer) {
		this.batoiLogicCustomer = batoiLogicCustomer;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "postal_code_id")
	public BatoiLogicPostalCode getBatoiLogicPostalCode() {
		return this.batoiLogicPostalCode;
	}

	public void setBatoiLogicPostalCode(BatoiLogicPostalCode batoiLogicPostalCode) {
		this.batoiLogicPostalCode = batoiLogicPostalCode;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "create_uid")
	public Integer getCreateUid() {
		return this.createUid;
	}

	public void setCreateUid(Integer createUid) {
		this.createUid = createUid;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 29)
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "write_uid")
	public Integer getWriteUid() {
		return this.writeUid;
	}

	public void setWriteUid(Integer writeUid) {
		this.writeUid = writeUid;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "write_date", length = 29)
	public Date getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "batoiLogicAddress")
	public Set<BatoiLogicOrder> getBatoiLogicOrders() {
		return this.batoiLogicOrders;
	}

	public void setBatoiLogicOrders(Set<BatoiLogicOrder> batoiLogicOrders) {
		this.batoiLogicOrders = batoiLogicOrders;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "batoiLogicAddress")
	public Set<BatoiLogicBill> getBatoiLogicBills() {
		return this.batoiLogicBills;
	}

	public void setBatoiLogicBills(Set<BatoiLogicBill> batoiLogicBills) {
		this.batoiLogicBills = batoiLogicBills;
	}

	@Override
	public String toString() {
		return name;
	}
	
}
